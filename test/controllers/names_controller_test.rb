require 'test_helper'

class NamesControllerTest < ActionController::TestCase
  test "should get generate" do
    get :generate
    assert_response :success
  end

  test "should get generate_full_name" do
    get :generate_full_name
    assert_response :success
  end

  test "should get generate_name" do
    get :generate_name
    assert_response :success
  end

end
