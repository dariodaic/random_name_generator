# Be sure to restart your server when you modify this file.

NameGenerator::Application.config.session_store :cookie_store,
	key: '_name_generator_session',
	expire_after: 5.minutes
