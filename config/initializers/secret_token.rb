# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
NameGenerator::Application.config.secret_key_base = '1fdb5b7fd4d062db40e6371eed5f83ecda744cc00afe9b69dcd49cb04bb3ed76d486a9fd8cf879ceb478f6fef14e1cc080e87a476a32c58c1c16f11173c486e6'
