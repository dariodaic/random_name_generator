class NamesController < ApplicationController
	before_action :collect_names, only: [:sort_out]

	def homepage
		if valid_params?
			@first_num = params[:first_names_number]
			@first_letter = params[:first_names_letter]
			@last_num = params[:surnames_number]
			@last_letter = params[:surnames_letter]
			@name = Name.new
			generated_name = @name.generate_full_name(@first_num, @first_letter, @last_num,	@last_letter)

			if session[:collected_names]
				if session[:collected_names].count < 10
					session[:collected_names].unshift(generated_name)
				else
					session[:collected_names].pop
					session[:collected_names].unshift(generated_name)
				end
			else
				session[:collected_names] = Array.new.unshift(generated_name)
			end
		end
	end

	def generated_names
		# session[:collected_names] = nil
		# clear names in a session using the upper statement
		if session[:collected_names]
			collect_names

			if @collected_names.size > 1
				@sorted = Name.sorted_names?(@collected_names)
				@names_count= session[:collected_names].size
			else
				@one_name = session[:collected_names][0]
			end
		end
	end

	def generate_love
		name = params[:name]
		alpha = params[:alpha_name]
		beta = params[:beta_name]

		if name
			if name == "alpha"
				@alpha = Name.new.generate_full_name("Random", "Random", "Random", "Random")
				session[:alpha] = @alpha
				session[:beta] ? @beta = session[:beta] : @beta = ""
			else
				@beta = Name.new.generate_full_name("Random", "Random", "Random", "Random")
				session[:beta] = @beta
				session[:alpha] ? @alpha = session[:alpha] : @alpha = ""
			end
		elsif alpha || beta
			if alpha.blank? && beta.blank?
				cash_values(alpha, beta)
				set_cashed_values
				@both_empty = true
			elsif alpha.blank?
				cash_values(alpha, beta)
				set_cashed_values
				@beta = beta
				@no_alpha = true
			elsif beta.blank?
				cash_values(alpha, beta)
				set_cashed_values
				@alpha = alpha
				@no_beta = true
			else
				@alpha = alpha
				@beta = beta
				cash_values(@alpha, @beta)
				@love_result = Name.calculate_that_love(@alpha, @beta)
				@love = "#{@love_result.to_s}%"
				@calculated = true
			end
		else
			set_cashed_values
		end
	end

	def sort_out
		name_part = params[:name_part]
		sort = params[:sort]

		if name_part == "first_name" && sort == "ascend"
			@collected_names = Name.sort_it(@collected_names, first_name: true, ascend: true)
		elsif name_part == "first_name" && sort == "descend"
			@collected_names = Name.sort_it(@collected_names, first_name: true, ascend: false)
		elsif name_part == "last_name" && sort == "ascend"
			@collected_names = Name.sort_it(@collected_names, first_name: false, ascend: true)
		elsif name_part == "last_name" && sort == "descend"
			@collected_names = Name.sort_it(@collected_names, first_name: false, ascend: false)
		end

		@sorted = Name.sorted_names?(@collected_names)
		@names_count = session[:collected_names].count

		render 'generated_names'
	end

	private

	def cash_values(alpha, beta)
		session[:alpha] = alpha
		session[:beta] = beta
	end

	def set_cashed_values
		alpha = session[:alpha]
		beta = session[:beta]

		@alpha = alpha ? alpha : ""
		@beta = beta ? beta : ""
	end

	def collect_names
		@collected_names = session[:collected_names].inject([]) do |memo, full_name|
			memo << Name.split_name_and_surname(full_name)
		end
	end

	def valid_params?
		valid_values = [(1..2), ('a'..'z'), "Random"]
		check_keys = [
			:first_names_number,
			:surnames_number,
			:first_names_letter,
			:surnames_letter
		]

		check_keys.each { |check_key| return false if params[check_key].nil? }

		first_names_num = params[:first_names_number]
		first_names_num = first_names_num.to_i if first_names_num != valid_values[2]
		surnames_num = params[:surnames_number]
		surnames_num = surnames_num.to_i if surnames_num != valid_values[2]
		first_names_letter = params[:first_names_letter]
		surnames_letter = params[:surnames_letter]

		if valid_values[0].include?(first_names_num) || first_names_num == valid_values[2]
			if valid_values[0].include?(surnames_num) || surnames_num == valid_values[2]
				if valid_values[1].include?(first_names_letter) || first_names_letter == valid_values[2]
					if valid_values[1].include?(surnames_letter) || surnames_letter == valid_values[2]
						return true
					end
				end
			end
		end

		false
	end
end