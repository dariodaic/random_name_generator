module NamesHelper
	def alphabet_values
		('A'..'Z').inject([]) { |memo, letter| memo << [letter, letter.downcase] }.unshift(["Random", "Random"])
	end

	def name_number_values(first_number, last_number)
		(first_number..last_number).inject([]) { |memo, number| memo << [number.to_s, number] }.unshift(["Random", "Random"])
	end

	def anchor(path)
		path + "#anchor"
	end
end
