class Name < ActiveRecord::Base

	class <<self
		def test_full_name # OK
			one_name = 0
			two_names = 0
			one_surname = 0
			two_surnames = 0
			name = self.new

			1000.times do
				full_name = name.generate_full_name("Random", "Random", "Random", "Random").split(" ")
				double_surname = full_name.last.split("-").count == 2 ? true : false

				if full_name.count == 2 && !double_surname
					one_name += 1
					one_surname += 1
				elsif full_name.count == 2 && double_surname
					one_name += 1
					two_surnames += 1
				elsif full_name.count == 3 && !double_surname
					two_names += 1
					one_surname += 1
				elsif full_name.count == 3 && double_surname
					two_names += 1
					two_surnames += 1
				end
			end

			{ one_name: one_name,
				two_names: two_names,
				one_surname: one_surname,
				two_surnames: two_surnames }
		end

		def calculate_that_love(alpha_name, beta_name) # OK
			alpha_val = alpha_name.split("").inject(0) { |memo, char| memo += char.ord }
			beta_val = beta_name.split("").inject(0) { |memo, char| memo += char.ord }

			ascii_result = alpha_val > beta_val ? (alpha_val - beta_val) : (beta_val - alpha_val)
			love_value = ascii_result % 100
			love_value != 0 ? love_value : 100
		end

		def split_name_and_surname(full_name) # OK
			full_name = full_name.split(" ")
			double_surname = full_name.last.split("-").count == 2 ? true : false

			if full_name.count == 2
				{ first_name: "#{full_name[0]}", last_name: "#{full_name[1]}"}
			elsif full_name.count == 3 && !double_surname
				{ first_name: "#{full_name[0]} #{full_name[1]}", last_name: "#{full_name[2]}" }
			elsif full_name.count == 3 && double_surname
				{ first_name: "#{full_name[0]} #{full_name[1]}", last_name: "#{full_name[2]}" }
			end
		end

		def sorted_names?(name_collection) # OK
			first_names = []
			surnames = []
			sorted = {
				alpha_asc: false,
				alpha_desc: false,
				beta_asc: false,
				beta_desc: false
			}

			name_collection.each do |full_name|
				first_names << full_name[:first_name].downcase
				surnames << full_name[:last_name].downcase
			end

			if first_names === first_names.sort
				sorted[:alpha_asc] = true
			elsif first_names === first_names.sort.reverse
				sorted[:alpha_desc] = true
			end

			if surnames === surnames.sort
				sorted[:beta_asc] = true
			elsif surnames === surnames.sort.reverse
				sorted[:beta_desc] = true
			end

			sorted
		end

		def sort_it(array, sort_info) # OK
			loop do
				sorted = true
				i = 0
				j = 1

				(array.length - 1).times do
					if sort_info[:ascend]
						if sort_info[:first_name]
							if array[i][:first_name].downcase > array[j][:first_name].downcase
								k = array[j]
								array[j] = array[i]
								array[i] = k
								sorted = false
							end
						else
							if array[i][:last_name].downcase > array[j][:last_name].downcase
								k = array[i]
								array[i] = array[j]
								array[j] = k
								sorted = false
							end
						end
					else
						if sort_info[:first_name]
							if array[i][:first_name].downcase < array[j][:first_name].downcase
								k = array[j]
								array[j] = array[i]
								array[i] = k
								sorted = false
							end
						else
							if array[i][:last_name].downcase < array[j][:last_name].downcase
								k = array[i]
								array[i] = array[j]
								array[j] = k
								sorted = false
							end
						end
					end
					i += 1
					j += 1
				end

				break if sorted
			end

			array
		end
	end

	def generate_full_name(name_count, name_start_letter, surname_count, surname_start_letter) # OK
		name_count = is_random?(name_count)
		name_start_letter = is_random?(name_start_letter)
		surname_count = is_random?(surname_count)
		surname_start_letter = is_random?(surname_start_letter)

		alphabet_array = ('a'..'z').to_a
		alphabet_length = alphabet_array.length

		if name_count == "Random" # 9:91
			(1..9).include?(rand(1..100)) ? name_count = 2 : name_count = 1
		end
		name_start_letter = alphabet_array[rand(0..alphabet_length)] if name_start_letter == "Random"

		if surname_count == "Random" # 5:91
			(1..5).include?(rand(1..100)) ? surname_count = 2 : surname_count = 1
		end
		surname_start_letter = alphabet_array[rand(0..alphabet_length)] if surname_start_letter == "Random"

		if name_count == 1
			name = "#{name_start_letter}"
		else
			first_name, second_name = "#{name_start_letter}", "#{name_start_letter}"
		end

		if surname_count == 1
			surname = "#{surname_start_letter}"
		else
			first_surname, second_surname = "#{surname_start_letter}","#{surname_start_letter}"
		end

		if name && surname
			self.generated = "#{generate_name(name)} #{generate_name(surname)}"
		elsif name && first_surname
			self.generated = "#{generate_name(name)} #{generate_name(first_surname)}-#{generate_name(second_surname)}"
		elsif first_name && surname
			self.generated = "#{generate_name(first_name)} #{generate_name(second_name)} #{generate_name(surname)}"
		elsif first_name && first_surname
			self.generated = "#{generate_name(first_name)} #{generate_name(second_name)} #{generate_name(first_surname)}-#{generate_name(second_surname)}"
		end
	end

	private

	def is_random?(value) # OK
		(value == "Random" || ('a'..'z').include?(value)) ? value : value.to_i
	end

	def generate_name(base_string) # OK
		name_length = rand(3..10)
		possible_letter_type = ["consonant", "vowel"]
		vowels = ['a', 'e', 'i', 'o', 'u']
		consonants = ('a'..'z').to_a - vowels

		vowels.include?(base_string[0]) ? vowel_count = 1 : cons_count = 1

		if vowels.include?(base_string[0])
			vowel_count = 1
			cons_count = 0
		else
			vowel_count = 0
			cons_count = 1
		end

		name_length.times do
			if vowel_count == 1
				base_string << consonants[rand(0..consonants.length - 1)]
				cons_count += 1
				vowel_count = 0
			elsif (cons_count == 2 && name_length > 3) || (cons_count == 1 && name_length == 3)
				base_string << vowels[rand(0..vowels.length - 1)]
				vowel_count += 1
				cons_count = 0
			else
				letter_type = possible_letter_type[rand(0..1)]

				if letter_type == "consonant"
					base_string << consonants[rand(0..consonants.length - 1)]
					cons_count += 1
					vowel_count = 0
				elsif letter_type == "vowel"
					base_string << vowels[rand(0..vowels.length - 1)]
					vowel_count += 1
					cons_count = 0
				end
			end
		end

		base_string.capitalize
	end

end
