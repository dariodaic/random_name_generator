class AddGeneratedToNames < ActiveRecord::Migration
  def change
    add_column :names, :generated, :string
  end
end
