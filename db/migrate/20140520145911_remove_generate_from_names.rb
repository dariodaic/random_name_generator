class RemoveGenerateFromNames < ActiveRecord::Migration
  def change
    remove_column :names, :generate, :string
  end
end
